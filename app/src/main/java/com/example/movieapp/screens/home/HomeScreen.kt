package com.example.movieapp.screens.home

import android.util.Log
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.example.movieapp.MovieRow
import com.example.movieapp.navigation.MovieScreens

@Composable
fun HomeScreen(navController: NavController){
    Scaffold(
        topBar = {
            Card(  modifier = Modifier
                .fillMaxWidth()
                .height(50.dp),
                shape = RoundedCornerShape(corner = CornerSize(30.dp)),
                elevation = 6.dp) {

                TopAppBar(
                    backgroundColor = Color.LightGray,
                    elevation = 10.dp,
                    modifier = Modifier.height(100.dp)

                ) {
                    Text(text = "Movies", modifier = Modifier.padding(start = 50.dp))

                }
            }

        }
    ) {
        MainContent(navController = navController)
    }

}
@Composable
fun MainContent(
    navController: NavController,
    movieList: List<String> = listOf(
        "Avatar",
        "300",
        "Harry Potter",
        "Lego: Movie",
        "Titanic",
        "Jumanji",
        "Justice League",
        "Iron Man",
        "Batman",
        "Life"
    )
) {
    Column(modifier = Modifier.padding(start = 12.dp, end = 12.dp)) {
        LazyColumn {
            items(items = movieList) {
                MovieRow(movie = it) { movie ->
                    navController.navigate(route = MovieScreens.DetailsScreen.name+"/$movie")
                }
            }
        }
    }
}